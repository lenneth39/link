# リンク集

## Intro
よく使うWEBリンク集  
適当に更新  

## Intra
- [ddreams トップページ](http://portal.ddreams.jp/) 
- [ddreams 社員録](http://dir.ddreams.jp/cgi-bin/dkeindx.cgi)  
- [ddreams スケジュール](https://web.drm.ddreams.jp/drm/api#/)  
- [ソリューションビジネスサイト](http://www.sol-web.sb.bch.west.ntt.co.jp/sb/)  
- [ビジネス営業ポータルサイト](http://bshp.west.ntt.co.jp/)  
- [責任内規](http://bshp.west.ntt.co.jp/wordpress/wp-content/uploads/page/rule/bieihon-sekinin-naiki-1-1.pdf)
- [GFO14Fの複合機](http://10.56.56.211/scan.htm)  
- [Redmine](https://10.56.56.230/login) 
- [Teams](https://teams.microsoft.com)
- [WebEX](https://www.webex.com/ja/index.html)
- [ELGANA](https://ncs.nttcom.biz/web/EBRxn8nzNV/home)  
- [CATS](http://cats.hq.west.ntt.co.jp/potal/apps/ZA01/ZA01E01.php)
- [N-bizlifestation(内部用)](https://web.n-bizlifestation.aincs.e-trans/gw/gw.html)
- [RMS](https://rms-to.ntt-west.local/RmsComGui/)
- [タレマネ](https://hcm44.sapsf.com/sf/home?bplte_company=nttcomware#Shell-home)
- [N-activeのユーザ変更するやつ](https://nactive.ntt-neo.co.jp/nauser/app/010101/010101_ctrl.php)

## AzSHub(Prod)
- [KVM(10.200.0.31)](https://10.200.0.31)

## Testbed
- [SSL-VPN(運用管理)](https://tb-sslmgt01.fortiddns.com)
- [SSL-VPN(閉域)](https://tb-sslclosed01.fortiddns.com)
- [SSL-VPN(テナント1)](https://tb-ssl01.fortiddns.com)								
- [SSL-VPN(テナント2)](https://tb-ssl02.fortiddns.com)									
- [SSL-VPN(テナント3)](https://tb-ssl03.fortiddns.com)									
- [SSL-VPN(テナント4)](https://tb-ssl04.fortiddns.com)									
- [SSL-VPN(テナント5)](https://tb-ssl05.fortiddns.com)
- [閉域ESXi#1 Webclient(172.16.180.11)](https://172.16.180.11)
- [閉域ESXi#1 iLO(172.16.180.12)](https://172.16.180.12)
- [閉域ESXi#2 Webclient(172.16.180.13)](https://172.16.180.13)
- [閉域ESXi#2 iLO(172.16.180.14)](https://172.16.180.14)
- [運用管理ESXi Webclient(172.16.180.1)](https://172.16.180.1)
- [運用管理ESXi iLO(172.16.180.2)](https://172.16.180.2)
- [運用管理Zabbix(172.16.180.3)](https://172.16.180.3/zabbix)
- [Fortigate 100E(172.16.180.254)](https://172.16.180.254)
- [Fortigate 1000D(172.16.180.250)](https://172.16.180.250)
- [Azure portal](https://portal.azure.com)
- [Azure Stack Hub User portal(Testbed)](https://portal.tb.azs.ntt-west.cloud)
- [Azure Stack Hub Admin portal(Testbed)](https://adminportal.tb.azs.ntt-west.cloud)

## Kyoto Trial
- [SSL-VPN](https://cstrkyoto.fortiddns.com:10443)
- [Fortigate 100E(192.168.0.4)](https://192.168.0.4)
- [Aruba 2530-24 #1(192.168.0.2)](https://192.168.0.2)
- [Aruba 2530-24 #2(192.168.0.3)](https://192.168.0.3)
- [Azure Stack Hub User Portal(kyoto)](https://portal.kyoto.ntt-dc.site/)

## Others
- [Time Tree](https://timetreeapp.com/signin?locale=ja)
- [生活者ドリブンマーケティング](https://seikatsusha-ddm.com/)
- [N-bizlifestation(外部用)](https://web.n-bizlifestation.jp/top/pn/PNA01S01/index?)
- [日経](https://www.nikkei.com)
- [東洋経済](https://toyokeizai.net)
- [AWS SAPの問題集](https://www.examtopics.com/exams/amazon/aws-certified-solutions-architect-professional/view/)
- [地域創生クラウド公式HP](https://www.ntt-west.co.jp/business/solution/rrc/)
- [gitlab](https://gitlab.com)
- [AWS Management Console](https://aws.amazon.com/jp/console/)
- [Azure Stack HubのInfraVMメモ](https://blog.aimless.jp/archives/2018-12-05-sizing-for-azure-stack/)
- [Covid19](<https://drive.google.com/drive/folders/18VCwCBUCdyup2odJ1foQ_brrdMw59QrR?usp=sharing>)

