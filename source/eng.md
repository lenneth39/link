# よく使いそうな英語

## intro
よく使いそうな英文をまとめておいておく

## あいさつ

| English | Japanese |
| ------ | ------ |
| I’m looking forward to hearing from you. | ご連絡をお待ちしています |
| I look forward to working with you again in the future. | 今後ともまた一緒にお仕事するのを楽しみにしています |
| Nice to meet you| はじめまして |
| It's pleasure to meet you | お会いできて光栄です  |
| It was nice meeting you | お会いできてよかったです（帰り際） |
| I’m looking forward to working with you | 一緒にお仕事ができるのを楽しみにしています |
| Thank you so much for your help in advance | 助けてくれて本当にありがとうございます！ |
| Thank you for your hardwork | ご対応ありがとうございました！ |
| Thank you for your great support | ご対応ありがとうございました！ |
| Thank you for your cooperation | ご協力ありがとうございます！ |

 
